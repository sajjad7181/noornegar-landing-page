$(document).ready(function () {

    var end = $('.count-down').attr('data-end');

    jQuery('.count-down').countdown("" + end + "", function(event) {
        var totalHours = event.offset.totalDays * 24 + event.offset.hours;
        jQuery(this).html(
            event.strftime('<div class="day"><label>' + event.offset.totalDays + '</label><span>روز</span></div><div class="hours"><label>' + event.offset.hours + '</label><span>ساعت</span></div><span class="dot">:</span><div class="minutes"><label>%M</label><span>دقیقه</span></div><span class="dot">:</span><div class="seconds"><label>%S</label><span>ثانیه</span></div>')
        );
    });

});